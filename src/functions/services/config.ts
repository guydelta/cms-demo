import { CosmosClient } from '@azure/cosmos';

const endpoint = process.env.CORE_API_URL;
const masterKey = process.env.CORE_API_KEY;
const databaseDefName = 'communities-db';
const communityContainerName = 'communities';

const client = new CosmosClient({ endpoint, key: masterKey });

//Add additional containers with appropriate container names here, e.g. properties, families, owners etc.
const containers = {
  communities: client.database(databaseDefName).container(communityContainerName),
};

export default containers;
