import { Context } from '@azure/functions';
import containers from './config';
import { hostname } from 'os';
const { communities: container } = containers;

interface Community {
  id: string;
  name: string;
  latitude?: number;
  longitude?: number;
  location?: string;
  mapString?: string;
  hoaName?: string;
  hoaRegistration?: string;
  hoaEmail?: string;
  hoaTel?: string;
}

async function getCommunity({ req, res }: Context) {
  try {
    const partitionKey = req.body.id;
    const { resource } = await container.item(partitionKey, partitionKey).read();
    res.status(200).json(resource);
  } catch (error) {
    res.status(500).send(error);
  }
}

async function getCommunities({ req, res }: Context) {
  try {
    const { resources: Communities } = await container.items.readAll().fetchAll();
    res.status(200).json(Communities);
  } catch (error) {
    res.status(500).send(error);
  }
}

async function postCommunity({ req, res }: Context) {
  const Community: Community = {
    id: undefined,
    name: req.body.name,
    location: req.body.location,
    latitude: req.body.latitude,
    longitude: req.body.longitude,
    mapString: req.body.mapString,
    hoaEmail: req.body.hoaEmail,
    hoaName: req.body.hoaName,
    hoaRegistration: req.body.hoaRegistration,
    hoaTel: req.body.hoaTel
  };
  Community.id = `Community${Community.name}`;

  try {
    const { item } = await container.items.create(Community);
    const { resource } = await item.read();
    res.status(201).json(resource);
  } catch (error) {
    res.status(500).send(error);
  }
}

async function putCommunity({ req, res }: Context) {
  const Community = {
    id: req.params.id,
    name: req.body.name,
    location: req.body.location,
    latitude: req.body.latitude,
    longitude: req.body.longitude,
    mapString: req.body.mapString,
    hoaEmail: req.body.hoaEmail,
    hoaName: req.body.hoaName,
    hoaRegistration: req.body.hoaRegistration,
    hoaTel: req.body.hoaTel
  };

  try {
    const partitionKey = Community.id;
    const { resource } = await container.item(Community.id, partitionKey).replace(Community);
    res.status(200).json(resource);
  } catch (error) {
    res.status(500).send(error);
  }
}

async function deleteCommunity({ req, res }: Context) {
  const { id } = req.params;

  try {
    const partitionKey = id;
    const { resource } = await container.item(id, partitionKey).delete();
    res.status(200).json(resource);
  } catch (error) {
    res.status(500).send(error);
  }
}

export default { getCommunity, getCommunities, postCommunity, putCommunity, deleteCommunity };
