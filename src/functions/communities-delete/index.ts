import { AzureFunction, Context, HttpRequest } from "@azure/functions";
import { communityService } from '../services';

const httpTrigger: AzureFunction = async function (context: Context, req: HttpRequest): Promise<void> {
  await communityService.deleteCommunity(context);
};

export default httpTrigger;
