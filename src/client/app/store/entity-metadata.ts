import { EntityMetadataMap } from '@ngrx/data';

//This file specify the data to be served using Ngrx patterns as recommended by the angular dev team and the video by John Papa on angular patterns
//This pattern greatly reduces the boilerplate for talking to external services
//Entities to be added can be of type community, property, owner, family etc. for the demo

const entityMetadata: EntityMetadataMap = {
  Community: {}
};

const pluralNames = { Community: 'Communities' };

export const entityConfig = {
  entityMetadata,
  pluralNames
};
