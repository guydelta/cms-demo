import { DefaultDataServiceConfig } from '@ngrx/data';
import { environment } from './../../environments/environment';

const root = environment.API;

export const defaultDataServiceConfig: DefaultDataServiceConfig = {
  root, // default root path to the server's web api

  entityHttpResourceUrls: {
    //Specify each of the data entity types here, e.g. properties, communities, families etc.
    Community: {
      // You must specify the root as part of the resource URL.
      entityResourceUrl: `${root}/communities/`,
      collectionResourceUrl: `${root}/communities/`
    }
  }
};
