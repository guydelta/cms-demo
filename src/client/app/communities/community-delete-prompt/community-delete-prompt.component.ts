import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Community } from '../../core';

@Component({
  selector: 'cms-community-delete-prompt',
  templateUrl: './community-delete-prompt.component.html',
  styleUrls: ['./community-delete-prompt.component.scss']
})
export class CommunityDeletePromptComponent {

  constructor(public dialogRef: MatDialogRef<CommunityDeletePromptComponent>, @Inject(MAT_DIALOG_DATA) public community: Community) { }

  onNoClick(): void {
    this.dialogRef.close();
  }



}
