import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunityDeletePromptComponent } from './community-delete-prompt.component';

describe('CommunityDeletePromptComponent', () => {
  let component: CommunityDeletePromptComponent;
  let fixture: ComponentFixture<CommunityDeletePromptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunityDeletePromptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunityDeletePromptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
