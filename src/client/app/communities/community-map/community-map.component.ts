import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Community } from '../../core';

@Component({
  selector: 'cms-community-map',
  templateUrl: './community-map.component.html',
  styleUrls: ['./community-map.component.scss']
})
export class CommunityMapComponent implements OnInit {

  center = { lat: 24, lng: 12 };
  // markerPositions: google.maps.LatLngLiteral[] = [];
  markerOptions = { draggable: false, position: { lat: 24, lng: 12 } };
  mapOptions: google.maps.MapOptions = {};
  zoom = 15;

  constructor(public dialogRef: MatDialogRef<CommunityMapComponent>, @Inject(MAT_DIALOG_DATA) public community: Community) {
    console.log(community);
    this.mapOptions = {
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    this.center.lat = parseFloat(community.latitude as any);
    this.center.lng = parseFloat(community.longitude as any);
    this.markerOptions.position = this.center;

    // this.markerPositions = [this.center];

  }

  ngOnInit(): void {

  }

}
