import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';

import { CommunitiesRoutingModule } from './communities-routing.module';
import { CommunitiesComponent } from './communities/communities.component';
import { CommunityDataService } from './community-data.service';
import { CommunityDeletePromptComponent } from './community-delete-prompt/community-delete-prompt.component';
import { CommunityComponent, CommunityDialogComponent } from './community/community.component';
import { CommunityMapComponent } from './community-map/community-map.component';


@NgModule({
  declarations: [CommunitiesComponent, CommunityDeletePromptComponent, CommunityComponent, CommunityDialogComponent, CommunityMapComponent],
  providers: [CommunityDataService],
  imports: [
    CommonModule,
    CommunitiesRoutingModule,
    SharedModule,
    CoreModule
  ]
})
export class CommunitiesModule { }
