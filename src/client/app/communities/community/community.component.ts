import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable, Subscription } from 'rxjs';

import { CommunityDataService } from '../community-data.service';
import { Community } from '../../core';

@Component({
  template: '',
})
export class CommunityComponent implements OnInit {
  id: string;
  community: Community;
  addMode: boolean = false;
  entityReference: Subscription;

  constructor(public dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private communityDataService: CommunityDataService) { }

  ngOnInit() {
    //Subscribed to the route param to get the id, even if we change the url manually
    //We don't need to unsubscribe from route param as angular takes care of that
    this.route.params.subscribe((params: Params) => {
      //The id will determine the future action of the dialog
      this.addMode = params["id"] === "new";
      this.id = params["id"];

      if (this.addMode) {
        this.community = new Community(null, 'New');
        this.openDialog();
        return;
      }

      //The param is an id, we will attempt to retrieve the record
      //We will have to assign the entity subscription to a variable to handle its lifecyle through to ngOnDestroy
      this.entityReference = this.communityDataService.entities$.subscribe(entities => {
        if (!entities.length) return this.router.navigate(['../'], { relativeTo: this.route });

        const found = entities.filter(entity => entity.id === this.id);

        //Bail since the entity does not exist
        if (!found.length) return this.router.navigate(['../'], { relativeTo: this.route });

        this.community = found[0];
        this.openDialog();
      });
    });
  }

  ngOnDestroy() {
    //If we dont do this the entity will keep updating itself and keep throwing a popup
    this.entityReference?.unsubscribe();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CommunityDialogComponent, { width: '550px', data: this.community });
    dialogRef.afterClosed().subscribe((community: Community) => {
      console.log(community);
      //Result is set means the dialog was accepted
      if (community) {
        if (this.addMode) {
          this.communityDataService.add(community);
        } else {
          this.communityDataService.update(community);
        }
      }
      //TODO: Always navigate back to communities unless there was a failure
      this.router.navigate(['../'], { relativeTo: this.route });
    });

  }

}


@Component({
  selector: 'cms-community',
  templateUrl: './community.component.html',
  styleUrls: ['./community.component.scss']
})
export class CommunityDialogComponent {
  public community;
  constructor(public dialogRef: MatDialogRef<CommunityDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: Community) {
    this.community = Object.assign({}, data);

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
