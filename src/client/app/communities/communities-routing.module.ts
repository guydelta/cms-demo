import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommunitiesComponent } from './communities/communities.component';
import { CommunityComponent } from './community/community.component';


const routes: Routes = [
  {
    path: '', pathMatch: 'prefix', component: CommunitiesComponent, children: [
      { path: ':id', pathMatch: 'full', component: CommunityComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommunitiesRoutingModule { }
