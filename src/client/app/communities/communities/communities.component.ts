import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';

//Services and models
import { CommunityDataService } from '../community-data.service';
import { Community } from '../../core';

//Components
import { CommunityDeletePromptComponent } from '../community-delete-prompt/community-delete-prompt.component';
import { CommunityMapComponent } from '../community-map/community-map.component';

@Component({
  selector: 'cms-communities',
  templateUrl: './communities.component.html',
  styleUrls: ['./communities.component.scss']
})
export class CommunitiesComponent implements OnInit {

  displayedColumns: string[] = ['edit', 'delete', 'name', 'location', 'mapString', 'hoaName', 'hoaRegistration', 'hoaEmail', 'hoaTel'];
  communities$: Observable<Community[]>;
  communities: Community[] = [];
  dataSource: MatTableDataSource<Community>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(public dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private communityDataService: CommunityDataService) {
    this.communities$ = this.communityDataService.entities$;

    this.dataSource = new MatTableDataSource(this.communities);

  }

  ngOnInit(): void {
    //Subscribing to the communities subscription will always keep it updated through ngrx
    this.communities$.subscribe(communities => {

      this.dataSource.data = communities;

      //Link the assisting components for the table to the data source
      //This has to be assigned every time the data is updated
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    //This initial call will keep the ngrx data in sync
    this.getCommunities();
  }

  getCommunities() {
    return this.communityDataService.getAll();
  }

  // Filter the data from a free-text field
  doApplyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    //When typing in filter, always ensure we go back to the first page of the table
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  doAddCommunity() {
    //Navigate to the edit window from the table without an id

    this.router.navigate(['./new'], { relativeTo: this.route });
  }

  doEditCommunity(community: Community) {
    // Navigate to the edit window from the table with an id
    this.router.navigate(['./' + community.id], { relativeTo: this.route });
  }

  doDeleteCommunity(community: Community) {
    // Delete the community on verification
    const dialogRef = this.dialog.open(CommunityDeletePromptComponent, {
      width: '250px',
      data: community
    });

    dialogRef.afterClosed().subscribe(result => {
      //Result will only be assigned when the delete dialog is confirmed
      if (!result) return;

      this.communityDataService.delete(result.id);
    });
  }

  doShowMap(community: Community) {
    // Show the community on maps dialog
    const dialogRef = this.dialog.open(CommunityMapComponent, {
      width: '450px',
      data: community
    });
  }
}
