import { Injectable } from '@angular/core';
import {
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory
} from '@ngrx/data';
// import { HttpClient } from '@angular/common/http';
import { Community } from '../core';

@Injectable({ providedIn: 'root' })
export class CommunityDataService extends EntityCollectionServiceBase<Community> {
  constructor(private serviceElementsFactory: EntityCollectionServiceElementsFactory) {
    //Super will expose the CRUD methods from the Ngrx store
    super('Community', serviceElementsFactory);
  }


  // Initial test
  // getCommunities() {
  //   return this.httpClient.get<Community[]>('./communities.json');
  // }

}
