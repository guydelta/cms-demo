export class Community {
  latitude?: number;
  longitude?: number;
  location?: string;
  mapString?: string;
  hoaName?: string;
  hoaRegistration?: string;
  hoaEmail?: string;
  hoaTel?: string;

  constructor(public id: string, public name: string) { };
}
